# FOSS4G-EU 2017 presentations

This repository aims to provide the presentations made during the
[FOSS4G Europe 2017](https://europe.foss4g.org/2017/Conference).

They are published with the authorization of their owners.

## Presentations

### Wed 19th

| 2017-07-19 |
|:-----|
| *Plenary session* |
| [OpeningPlenary - Gerald Fenoy](presentations/2017-07-19/foss4g-europe-2017-OpeningPlenary-GeraldFenoy.pdf) |
| [Copernicus, The European Unions flagship for climate and atmospheric analysis (Gold Sponsor) - Anabelle Guillory](presentations/2017-07-19/foss4g-europe-2017-gold-sponsor-Copernicus_The_European_Unions_flagship_for_climate_and_atmospheric_analysis-AnabelleGuillory.pdf) |
| [Tech trends and openess in the geospatial world - Athina Trakas (Keynote Speaker)](presentations/2017-07-19/foss4g-europe-2017-KeynoteSpeaker-Tech_trends_and_openess_in_the_geospatial_world-AthinaTrakas.pdf) |
| [Where Do We Come From - Venkatesh Raghavan (Keynote Speaker)](presentations/2017-07-19/foss4g-europe-2017-KeynoteSpeaker-Where_Do_We_Come_From-VenkateshRaghavan.pdf) |
| *General Track* |
| [A_cloudfree Europe with sentinel2 - JUngar, SMeissl](presentations/2017-07-19/general_track/foss4g-europe-2017-A_cloudfree_Europe_with_sentinel2-JUngar-SMeissl.pdf) |
| [Adressing referencing issues with new standards and introduction tot apache spatial information system SIS - Martin Desruisseaux](presentations/2017-07-19/general_track/foss4g-europe-2017-Adressing_referencing_issues_with_new_standards_and_introduction_tot_apache_spatial_information_system_SIS-MartinDesruisseaux.pdf) |
| [Advanced geospatial technologies the new powerfull GRASS GIS 7.2 release - Markus Neteler](presentations/2017-07-19/general_track/foss4g-europe-2017-Advanced_geospatial_technologies_the_new_powerfull_GRASS_GIS_7_2_release-MarkusNeteler.pdf) |
| [Albion Geological modelling software - Emmanuel Duguey](presentations/2017-07-19/general_track/foss4g-europe-2017-Albion_Geological_modelling_software-EmmanuelDuguey.pdf) |
| [Albion Geological modelling software - Artois (video)](presentations/2017-07-19/general_track/foss4g-europe-2017-Albion_Geological_modelling_software-Artois.avi) |
| [Albion Geological modelling software - Prototype Albion 2015 (video)](presentations/2017-07-19/general_track/foss4g-europe-2017-Albion_Geological_modelling_software-Prototype_Albion_2015.webm) |
| [Albion Geological modelling software - Usual Method (video)](presentations/2017-07-19/general_track/foss4g-europe-2017-Albion_Geological_modelling_software-UsualMethod.webm) |
| [A_series of unfortunate maps - JSanz, RAzonar](presentations/2017-07-19/general_track/foss4g-europe-2017-A_series_of_unfortunate_maps-JSanz-RAzonar.pdf) |
| [Assessment of the digital openness of the geospatial dimension - Codrina Maria Ilie](presentations/2017-07-19/general_track/foss4g-europe-2017-Assessment_of_the_digital_openness_of_the_geospatial_dimension-CodrinaMariaIlie.pdf) |
| [Assess the OSM data quality starting from contribution history - DGaraud, RDelhome, HMercier, OCourtin](presentations/2017-07-19/general_track/foss4g-europe-2017-Asses_the_OSM_data_quality_starting_from_contribution_history-DGaraud-RDelhome-HMercier-OCourtin.pdf) |
| [CARTO2017 - JSanz, EMartinez](presentations/2017-07-19/general_track/foss4g-europe-2017-CARTO2017-JSanz-EMartinez.pdf) |
| [EOxC - a modern web catalog client - FSchindler, SMeissl](presentations/2017-07-19/general_track/foss4g-europe-2017-EOxC_-_a_modern_web_catalog_client-FSchindler-StephanMeissl.pdf) |
| [Examining The Spatial Dynamics of Economic Globalisation : A_tale of using Open source technologies - Anthonia-IjeomaOnyeahiala](presentations/2017-07-19/general_track/foss4g-europe-2017-Examining_The_Spatial_Dynamics_of_Economic_Globalisation_A_tale_of_using_Open_source_technologies-Anthonia-IjeomaOnyeahialam.pdf) |
| [Four-letter word - Ivan-Sanchez Ortega](presentations/2017-07-19/general_track/foss4g-europe-2017-Four-letter_word-Ivan-SanchezOrtega.pdf) |
| [GDAL 2.2 What's new - Even Rouault](presentations/2017-07-19/general_track/foss4g-europe-2017-GDAL_2.2_Whats_new-EvenRouault.pdf) |
| [GeoNode the open source geospatial CMS - Alessio Fabiani](presentations/2017-07-19/general_track/foss4g-europe-2017-GeoNode_the_open_source_geospatial_CMS-AlessioFabiani.pdf) |
| [Inspiring European data providers through open source geospatial software - BDeRoo, DFrige, AKotsev, RSmith, MLutz](presentations/2017-07-19/general_track/foss4g-europe-2017-Inspiring_European_data_providers_through_open_source_geospatial_software-BDeRoo-DFrige-AKotsev-RSmith-MLutz.pdf) |
| [Lets integrate BIM and 3D GIS on top of FOSS4G - SShin, SSon, BJJang, JDCheon, HKim](presentations/2017-07-19/general_track/foss4g-europe-2017-Lets_integrate_BIM_and_3D_GIS_on_top_of_FOSS4G-SShin-SSon-BJJang-JDCheon-HKim.pdf) |
| [Lets integrate BIM and 3D GIS on top of FOSS4G - mago3d (video)](presentations/2017-07-19/general_track/foss4g-europe-2017-Lets_integrate_BIM_and_3D_GIS_on_top_of_FOSS4G-mago3d.mp4) |
| [Make it rain with Mapbox GL - FNeubert, TFreudl-Gierke](presentations/2017-07-19/general_track/foss4g-europe-2017-Make_it_rain_with_Mapbox_GL-FNeubert-TFreudl-Gierke.pdf) |
| [Melown 3D mapping stack - OProchazka, JCepicky, SSumbera](presentations/2017-07-19/general_track/foss4g-europe-2017-Melown_3D_mapping_stack-OProchazka-JCepicky-SSumbera.pdf) |
| [Melown 3D mapping stack - explore remote sensing data (video)](presentations/2017-07-19/general_track/foss4g-europe-2017-Melown_3D_mapping_stack-explore-remote-sensing-data.mkv) |
| [Melown 3D mapping stack - gis integrations (video)](presentations/2017-07-19/general_track/foss4g-europe-2017-Melown_3D_mapping_stack-gis-integrations.mp4) |
| [Melown 3D mapping stack - hiearchical labels with styles (video)](presentations/2017-07-19/general_track/foss4g-europe-2017-Melown_3D_mapping_stack-hiearchical-labels-with-styles.mp4) |
| [Melown 3D mapping stack - paris to boston (video)](presentations/2017-07-19/general_track/foss4g-europe-2017-Melown_3D_mapping_stack-paris-to-boston.mp4) |
| [Melown 3D mapping stack - stream globes (video)](presentations/2017-07-19/general_track/foss4g-europe-2017-Melown_3D_mapping_stack-stream-globes.mp4) |
| [Melown 3D mapping stack - stream true 3d landscapes (video)](presentations/2017-07-19/general_track/foss4g-europe-2017-Melown_3D_mapping_stack-stream-true3d-landscapes.mp4) |
| [Melown 3D mapping stack - vts racing android (video)](presentations/2017-07-19/general_track/foss4g-europe-2017-Melown_3D_mapping_stack-video-vts-racing-android.mp4) |
| [Mobile Application for 3D real-time visualization for outdoor sports competitions LIS3D - TPages, RBeuscart, JSoula, AQuesnel-Barbet](presentations/2017-07-19/general_track/foss4g-europe-2017-Mobile_Application_for_3D_real-time_visualization_for_outdoor_sports_competitions_LIS3D-TPages-RBeuscart-JSoula-AQuesnel-Barbet.pdf) |
| [Mobile Application for 3D real-time visualization for outdoor sports competitions LIS3D - SRCM (video)](presentations/2017-07-19/general_track/foss4g-europe-2017-Mobile_Application_for_3D_real-time_visualization_for_outdoor_sports_competitions_LIS3D-SRCM.mp4) |
| [Portable Antiquities of the Nederlands - NvanRuler, KSmallenbroek, MvanVeen, RLassche](presentations/2017-07-19/general_track/foss4g-europe-2017-Portable_Antiquities_of_the_Nederlands-NvanRuler-KSmallenbroek-MvanVeen-RLassche.pdf) |
| [Remonterletemps.ign.fr the_geospatial time machine built upon open source components - Thomas Tilak](presentations/2017-07-19/general_track/foss4g-europe-2017-Remonterletempsignfr_the_geospatial_time_machine_built_upon_oepn_source_components-ThomasTilak.pdf) |
| [Serving successfully millions of 3D objects in a browser - LGasser, OTerral](presentations/2017-07-19/general_track/foss4g-europe-2017-Serving_successfully_millions_of_3D_objects_in_a_browser-LGasser-OTerral.pdf) |
| [Serving successfully millions of 3D objects in a browser - hlod to lausanne mac 2017-07-05T15-17-36-470Z (video)](presentations/2017-07-19/general_track/foss4g-europe-2017-Serving_successfully_millions_of_3D_objects_in_a_browser-hlod-to-lausanne-mac-2017-07-05T15-17-36-470Z.webm) |
| [Serving successfully millions of 3D objects in a browser - labels styled 2017-07-13T07-32-02-227Z (video)](presentations/2017-07-19/general_track/foss4g-europe-2017-Serving_successfully_millions_of_3D_objects_in_a_browser-labels-styled-2017-07-13T07-32-02-227Z.webm) |
| [State of the art of WebGL map viewers - Ivan-Sanchez Ortega](presentations/2017-07-19/general_track/foss4g-europe-2017-State_of_the_art_of_WebGL_map_viewers-Ivan-SanchezOrtega.pdf) |
| [Struggling with the Open Community - Ilya Zverev](presentations/2017-07-19/general_track/foss4g-europe-2017-Struggling_with_the_Open_Community-IlyaZverev.pdf) |
| [Understanding the flexibility of Open Source - Jody Garnett](presentations/2017-07-19/general_track/foss4g-europe-2017-Understanding_the_flexibility_of_Open_Source-JodyGarnett.pdf) |
| [Whats new in Orfeo toolbox 6.0 - VPoughon, JMichel, GPasero, RCresson](presentations/2017-07-19/general_track/foss4g-europe-2017-Whats_new_in_Orfeo_toolbox_6_0-VPoughon-JMichel-GPasero-RCresson.pdf) |
| *Academic Track* |
| [Building a geographic data repository for urban research with free software - learning_from_observatorio.cedeus.cl - SSteinger, HdelaFuente, CFuentes-JBarton, J-CMunoz](presentations/2017-07-19/academic_track/foss4g-europe-2017-Building_a_geographic_data_repository_for_urban_research_with_free_software-learning_from_observatoriocedeuscl-SSteinger-HdelaFuente-CFuentes-JBarton-J-CMunoz.pdf) |
| [Can reconstructed landsurface temperature data from space predict a west nile virus outbreak - VAndreoa, MMetz, NNeteler, RRosa, MMarcantonio, CBillinis, ARizzoli, APapa](presentations/2017-07-19/academic_track/foss4g-europe-2017-Can_reconstructed_landsurface_temperature_data_from_space_predict_a_west_nile_virus_outbreak-VAndreoa-MMetz-NNeteler-RRosa-MMarcantonio-CBillinis-ARizzoli-APapa.pdf) |
| [High resolution global gridded data for use in population studies - CT Lloyd](presentations/2017-07-19/academic_track/foss4g-europe-2017-High_resolution_global_gridded_data_for_use_in_population_studies-CTLloyd.pdf) |
| [Processing big remote sensing data for fast flood detection in a distributed computing environment - AOlasz, DKristof, BNguyenThai, MBelenyesi, RGiachetta](presentations/2017-07-19/academic_track/foss4g-europe-2017-Processing_big_remote_sensing_data_for_fast_flood_detection_in_a_distributed_computing_environment-AOlasz-DKristof-BNguyenThai-MBelenyesi-RGiachetta.pdf) |
| [The extraction of indoor building information from BIM to OGC indoorGML - T-AnnTeo, SzChengYu](presentations/2017-07-19/academic_track/foss4g-europe-2017-The_extraction_of_indoor_building_information_from_BIM_to_OGC_indoorGML-T-AnnTeo-Sz-ChengYu.pdf) |

### Thu 20th

| 2017-07-20 |
|:-----|
| *Plenary session* |
| [United Nation OpenGIS Initiative - Kyoung Soo Eom, Maria Antinoa Brovelli(Keynote Speaker)](presentations/2017-07-20/foss4g-europe-2017-KeynoteSpeaker-United_Nation_OpenGIS_Initiative-Kyoung-SooEom-Maria-AntinoaBrovelli.pdf) |
| [Why Open Source Software the ESAs Exploitation Platforms experience - Salvatore Pinto (Keynote Speaker)](presentations/2017-07-20/foss4g-europe-2017-KeynoteSpeaker-Why_Open_Source_Software_the_ESAs_Exploitation_Platforms_experience-SalvatorePinto.pdf) |
| [GeoCat - Maria Arias de Reyna (Silver Sponsor)](presentations/2017-07-20/foss4g-europe-2017-silver-sponsor-GeoCat-Maria-Arias-deReyna.pdf) |
| [GeoSolutions : Building geospatial infrastructures with_GeoSolutions - Andrea Aime (Silver Sponsor)](presentations/2017-07-20/foss4g-europe-2017-silver-sponsor_GeoSolutions-Building_geospatial_infrastructures_with_GeoSolutions-AndreaAime.pdf) |
| [Melown VTS 3D Map streaming and rendering stack - Ondrej Procchazka (Silver Sponsor)](presentations/2017-07-20/foss4g-europe-2017-silver-sponsor-Melown_VTS_3D_Map_streaming_and_rendering_stack-OndrejProcchazka.pdf) |
| *General Track* |
| [10 years of OSGeo @GsoC what's_next ? - Margherita Di Leo](presentations/2017-07-20/general_track/foss4g-europe-2017-10_years_of_OSGeo_@_GsoC_whats_next-MargheritaDiLeo.pdf) |
| [D3js in PostGIS - Tom van Tilburg](presentations/2017-07-20/general_track/foss4g-europe-2017-D3js_in_PostGIS-Tom-vanTilburg.pdf) |
| [ESA NASA WebWorldWind : Status and perspective 2 years after its introduction at FOSS4G in Como - Yann Voumard](presentations/2017-07-20/general_track/foss4g-europe-2017-ESA_NASA_WebWorldWind_Status_and_perspective_2_years_after_its_introduction_at_FOSS4G_in_Como-YannVoumard.pdf) |
| [Estimating public transit real-time locations based on time-table data (video) - Tonis Kardi](presentations/2017-07-20/general_track/foss4g-europe-2017-Estimating_public_transit_real-time_locations_based_on_time-table_data-TonisKardi.mp4) |
| [IGEO geospatial platform - Marko Turkovic](presentations/2017-07-20/general_track/foss4g-europe-2017-IGEO_geospatial_platform-MarkoTurkovic.pdf) |
| [Implementing INSPIRE view and download services - transition_to_FOSS4G (video) - Tonis Kardi](presentations/2017-07-20/general_track/foss4g-europe-2017-Implementing_INSPIRE_view_and_download_services_-_transition_to_FOSS4G-TonisKardi.mp4) |
| [INSPIRE and Open Source Geospatial Solutions adding missing pieces to the puzzle - RSmith, ZKotsev, LHernandez, DFrigne, BDeRoo](presentations/2017-07-20/general_track/foss4g-europe-2017-INSPIRE_and_Open_Source_Geospatial_Solutions_adding_missing_pieces_to_the_puzzle-RSmith-ZKotsev-LHernandez-DFrigne-BDeRoo.pdf) |
| [In the service of democracy allocating expat voters to polling stations using FOSS_GIS - DUrda, FIosub](presentations/2017-07-20/general_track/foss4g-europe-2017-In_the_service_of_democracy_allocating_expat_voters_to_polling_stations_using_FOSS_GIS-DUrda-FIosub.pdf) |
| [Introducing hex-utils : an hexagonal raster toolkit - LMdeSousa, JPLeitao](presentations/2017-07-20/general_track/foss4g-europe-2017-Introducing_hex-utils_an_haxagonal_raster_toolkit-LMdeSousa-JPLeitao.pdf) |
| [Introducing the vehicule routing open source optimization machine (VROOM) - Julien Coupey](presentations/2017-07-20/general_track/foss4g-europe-2017-Introducing_the_vehicule_routing_open_source_optimization_machine_VROOM-JulienCoupey.pdf) |
| [Introducing the vehicule routing open source optimization machine (VROOM) - 1 (video)](presentations/2017-07-20/general_track/foss4g-europe-2017-Introducing_the_vehicule_routing_open_source_optimization_machine_VROOM-1.ogv) |
| [Introducing the vehicule routing open source optimization machine (VROOM) - 2 (video)](presentations/2017-07-20/general_track/foss4g-europe-2017-Introducing_the_vehicule_routing_open_source_optimization_machine_VROOM-2.ogv) |
| [Lets share GIS much quicker - MLanda, JCepicky, MDancak, IMincik](presentations/2017-07-20/general_track/foss4g-europe-2017-Lets_share_GIS_much_quicker-MLanda-JCepicky-MDancak-IMincik.pdf) |
| [MapStore2 modern mashups with OL3 Leaflet and React - Mauro Bartolomeoli](presentations/2017-07-20/general_track/foss4g-europe-2017-MapStore2_modern_mashups_with_OL3_Lefatlet_and_React-MauroBartolomeoli.pdf) |
| [New ASIG Geoportal : central place for spatial data and services in Albania - Marko Skvorc](presentations/2017-07-20/general_track/foss4g-europe-2017-New_ASIG_Geoportal_central_place_for_spatial_data_and_services_in_Albania-MarkoSkvorc.pdf) |
| [OGC INSPIRE versus OpenData LinkedData - Maria Arias de Reyna](presentations/2017-07-20/general_track/foss4g-europe-2017-OGC_INSPIRE_versus_OpenData_LinkedData-Maria-Arias-deReyna.pdf) |
| [OpenMapTiles Ready to use OpenStreetMap vector tiles - Petr Sloup](presentations/2017-07-20/general_track/foss4g-europe-2017-OpenMapTiles_Ready_to_use_OpenStreetMap_vector_tiles-PetrSloup.pdf) |
| [OSGeo Branding and Website Reboot - Jody Garnett](presentations/2017-07-20/general_track/foss4g-europe-2017-OSGeo_Branding_and_Website_Reboot-JodyGarnett.pdf) |
| [PyroSAR : a Python framework for large-scale SAR satellite data processing - JTruckbrodt, FCremer, JEberle, CSchullius](presentations/2017-07-20/general_track/foss4g-europe-2017-PyroSAR_a_Python_framework_for_large-scale_SAR_satellite_data_processing-JTruckbrodt-FCremer-JEberle-CSchullius.pdf) |
| [QGIS 3 Refactoring and enhancement - DMarteau, PBlottiere](presentations/2017-07-20/general_track/foss4g-europe-2017-QGIS_3_Refactoring_and_enhancement-DMarteau-PBlottiere.pdf) |
| [Shortest Path search in your database and more with pgRouting - Daniel Kastl](presentations/2017-07-20/general_track/foss4g-europe-2017-Shortest_Path_search_in_your_database_and_more_with_pgRouting-DanielKastl.pdf) |
| [The importance of user communities in the uptake of open Copernicus data and information - ADebien, SOurevitch, JTurpin](presentations/2017-07-20/general_track/foss4g-europe-2017-The_importance_of_user_communities_in_th_uptake_of_open_Copernicus_data_and_information-ADebien-SOurevitch-JTurpin.pdf) |
| [What's up with diversity - Maria Arias de Reyne](presentations/2017-07-20/general_track/foss4g-europe-2017-Whats_up_with_diversity-Maria-Arias-deReyne.mp4) |
| *Academic Track* |
| [A new digital image correlation software for displacements field measurement in structural applications - RRavanelli, ANascetti, MDiRita, VBelloni, DMattei, NNistico, MCrespi](presentations/2017-07-20/academic_track/foss4g-europe-2017-A_new_digital_iamge_correlation_software_for_displacements_field_measurement_in_structural_applications-RRavanelli-ANascetti-MDiRita-VBelloni-DMattei-NNistico-MCrespi.pdf) |
| [Concept and implementation of an architecture for the immediate provision of geodata in disaster management - SFleischhauer, FJBehr, PRawiel](presentations/2017-07-20/academic_track/foss4g-europe-2017-Concept_and_implementation_of_an_architecture_for_the_immediate_provision_of_geodata_in_disaster_management-SFleischhauer-FJBehr-PRawiel.pdf) |
| [Extension of rtklib for the calculation and validation of protection levels - BTakacs, ZSiki, RMarkovits-Somogyi](presentations/2017-07-20/academic_track/foss4g-europe-2017-Extension_of_rtklib_for_the_calculation_and_validation_of_protection_levels-BTakacs-ZSiki-RMarkovits-Somogyi.pdf) |
| [FOSS4G as key building block for case-based learning in_geographic information_education, MMinguini, MABrovelli, DVanderbroucke, MCarbonaro, SPruller, MPainho, GMartirano, DFrigne](presentations/2017-07-20/academic_track/foss4g-europe-2017-FOSS4G_as_key_building_block_for_case-based_learning_in_geographic_information_education-MMinguini-MABrovelli-DVanderbroucke-MCarbonaro-SPruller-MPainho-GMartirano-DFrigne.pdf) |
| [Integrating MCDM methods to GIS through a dedicated FOSS application - MABroveli, DGBSouza, C-AriasMunoz, DOxoli, MEMolinari](presentations/2017-07-20/academic_track/foss4g-europe-2017-Integrating_MCDM_methods_to_GIS_through_a_dedicated_FOSS_application-MABroveli-DGBSouza-C-AriasMunoz-DOxoli-MEMolinari.pdf) |
| [Online analysis of meteorological and climate geospatial datasets for northern eurasia environmental studies - ATitov, EGordov, IOkladnikov](presentations/2017-07-20/academic_track/foss4g-europe-2017-Online_analysis_of_meteorological_and_climate_geospatial_datasets_for_northern_eurasia_environmental_studies-ATitov-EGordov-IOkladnikov.pdf) |
| [Screening of environmental impact of pollution with the QGIS plugin evifate - FGeri, OCainelli, GSalogni, PZatelli, MCiolli](presentations/2017-07-20/academic_track/foss4g-europe-2017-Screening_of_environmental_impact_of_pollution_with_the_qgis_plugin_evifate-FGeri-OCainelli-GSalogni-PZatelli-MCiolli.pdf) |
| [UN Open GIS Capacity building - AAlbertella, MABrovelli, DGonzalez Ferreiro](presentations/2017-07-20/academic_track/foss4g-europe-2017-UN_Open_GIS_Capacity_building-AAlbertella-MABrovelli-DGonzalezFerreiro.pdf) |

### Fri 21st

| 2017-07-21 |
|:-----|
| *Plenary session* |
| [The future of Geo is open - Suchith Anand (Keynote Speaker)](presentations/2017-07-21/foss4g-europe-2017-KeynoteSpeaker-The_future_of_Geo_is_open-SuchithAnand.pdf) |
| [The future of Geo is open GODAN PSA - One solution for Zero Hunger (video)](presentations/2017-07-21/foss4g-europe-2017-KeynoteSpeaker-The_future_of_Geo_is_open_GODAN_PSA-One_solution_for_Zero_Hunger.mp4) |
| [Passport in my shoe - JeffMc Kenna (Keynote Speaker)](presentations/2017-07-21/foss4g-europe-2017-KeynoteSpeaker-Passport_in_my_shoe-JeffMcKenna.pdf) |
| [Closing Plenary - Gerald Fenoy](presentations/2017-07-21/foss4g-europe-2017-Closing_Plenary-GeraldFenoy.pdf) |
| [Closing Plenary - Group Shot 1](presentations/2017-07-21/foss4g-europe-2017-Closing_Plenary-GroupShot1.jpg) |
| [Closing Plenary_- Group Shot 2](presentations/2017-07-21/foss4g-europe-2017-Closing_Plenary_GroupShot2.jpg) |
| [Closing Plenary Tessie Is A Fish](presentations/2017-07-21/foss4g-europe-2017-Closing_Plenary_TessieIsAFish.pdf) |
| [Closing Plenary FOSS4G Boston](presentations/2017-07-21/foss4g-europe-2017-Closing_Plenary_FOSS4G_Boston.pdf) |
| [Closing Plenary FOSS4G 2018 Dar-es Salaam](presentations/2017-07-21/foss4g-europe-2017-Closing_Plenary_FOSS4G_2018_Dar-es-Salaam.pdf) |
| *General Track* |
| [A digital approach for forest and land use planning built on WMS WPS and WRSm - MRageade, ACCapel, CLardeux](presentations/2017-07-21/general_track/foss4g-europe-2017-A_digital_approach_for_forest_and_land_use_planning_built_on_WMS_WPS_and_WRSm-MRageade-A-CCapel-CLardeux.pdf) |
| [Advanced features in pyWPS 4.0.0 - JCepicky, LDeSousa](presentations/2017-07-21/general_track/foss4g-europe-2017-Advanced_features_in_pyWPS_4_0_0-JCepicky-LDeSousa.pdf) |
| [Angular2 Geo-Apps with YAGA - ASchubert, SHerritsch](presentations/2017-07-21/general_track/foss4g-europe-2017-Angular2_Geo-Apps_with_YAGA-ASchubert-SHerritsch.pdf) |
| [Aquadrone geo-tracking and collecting environmental data from an underwater remotely operated vehicule - ALiccardi, JCollomb](presentations/2017-07-21/general_track/foss4g-europe-2017-Aquadrone_geo-tracking_and_collecting_envrionmentaal_data_from_an_underwater_remotely_operated_vehicule-ALiccardi-JCollomb.pdf) |
| [Building a Table Joining Like Service with WPS - Ian Turton](presentations/2017-07-21/general_track/foss4g-europe-2017-Building_a_Table_Joining_Like_Service_with_WPS-IanTurton.mp4) |
| [Data Processing with QGIS3 - YJacolin](presentations/2017-07-21/general_track/foss4g-europe-2017-Data_Processing_with_QGIS3-YJacolin.pdf) |
| [FREEWAT platform for integrated water management - MCannata, RRossetto, IBorsi, RCriollo, LFoglia](presentations/2017-07-21/general_track/foss4g-europe-2017-FREEWAT_platform_for_integrated_water_management-MCannata-RRossetto-IBorsi-RCriollo-LFoglia.pdf) |
| [Frentrol.hu experiences of the open aerial photo archive - KTakacs, PBraunmuller](presentations/2017-07-21/general_track/foss4g-europe-2017-Frentrolhu_experiences_of_the_open_aerial_photo_archive-KTakacs-PBraunmuller.pdf) |
| [GeoMapFish : an Open Source WebGIS Project Status Report - YBolognini, YJacolin](presentations/2017-07-21/general_track/foss4g-europe-2017-GeoMapFish_an_Open_Source_WebGIS_Project_Status_Report-YBolognini-YJacolin.pdf) |
| [GeoNetwork : State of the Art - Maria Arias de Reyna](presentations/2017-07-21/general_track/foss4g-europe-2017-GeoNetwork_State_of_the_Art-Maria-Arias-deReyna.pdf) |
| [Hotspot Analysis an experimental python plugin to enable LISA mapping into QGIS - DOxoli, GPrestifilippo, MZurbaran](presentations/2017-07-21/general_track/foss4g-europe-2017-Hotspot_Analysis_an_experimental_python_plugin_to_enable_LISA_mapping_into_QGIS-DOxoli-GPrestifilippo-MZurbaran.pdf) |
| [Introducing mappyfile a python library for MapServer - Seth Girvin](presentations/2017-07-21/general_track/foss4g-europe-2017-Introducing_mappyfile_a_python_library_for_MapServer-SethGirvin.pdf) |
| [LiDAR analysis for hazard mapping and forestry management using JGrassTools and gvSIG - SFranceschi, A Antonello](presentations/2017-07-21/general_track/foss4g-europe-2017-LiDAR_analysis_for_hazard_mapping_and_forestry_management_using_JGrassTools_and_gvSIG-SFranceschi-AAntonello.pdf) |
| [Lizmap Feature Frenzy - MDouchin, R-LucD'Honts](presentations/2017-07-21/general_track/foss4g-europe-2017-Lizmap_Feature_Frenzy-MDouchin-R-LucDHonts.pdf) |
| [Mapbender3 Presentation and Status Report - Astrid Emde](presentations/2017-07-21/general_track/foss4g-europe-2017-Mapbender3_Presentation_and_Status_Report-AstridEmde.pdf) |
| [Mastering security with GeoServer and GeoFence - MBartolomeoli, ETajariol, SGiannecchini, AFabiani](presentations/2017-07-21/general_track/foss4g-europe-2017-Mastering_security_with_GeoServer_and_GeoFence-MBartolomeoli-ETajariol-SGiannecchini-AFabiani.pdf) |
| [Open data and FOSS4G to improve resilience for Comoros - ECattaneo, APogliaghi, VVenkatachalam](presentations/2017-07-21/general_track/foss4g-europe-2017-Open_data_and_FOSS4G_to_improve_resilience_for_Comoros-ECattaneo-APogliaghi-VVenkatachalam.pdf) |
| [Pirate Maps Experiments with portable map on the Raspberry Pi - Ian Turton](presentations/2017-07-21/general_track/foss4g-europe-2017-Pirate_Maps_Expriments_with_portable_map_on_the_Raspberry_Pi-IanTurton.mp4) |
| [Public Transport in GraphHopper - Michael Zilske](presentations/2017-07-21/general_track/foss4g-europe-2017-Public_Transport_in_GraphHopper-MichaelZilske.pdf) |
| [Serving Earth observation data with GeoServer addressing real world requirements - SGiannecchini, AAime](presentations/2017-07-21/general_track/foss4g-europe-2017-Serving_Earth_observation_data_with_GeoServer_addressing_real_world_requirements-SGiannecchini-AAime.pdf) |
| [State of GeoServer - AAime, JGarnett](presentations/2017-07-21/general_track/foss4g-europe-2017-State_of_GeoServer-AAime-JGarnett.pdf) |
| [Taking advantage of OGC and HTML5 - Offline web editing - Fernando Lacunza](presentations/2017-07-21/general_track/foss4g-europe-2017-Taking_advantage_of_OGC_and_HTML5_Offline_web_editing-FernandoLacunza.pdf) |
| [Taking advantage of OGC_and HTML5 - Offline web editing demo (video)](presentations/2017-07-21/general_track/foss4g-europe-2017-Taking_advantage_of_OGC_and_HTML5_Offline_web_editing-demo.mp4) |
| [The Geopaparazzi Project State of the art - AAntonello, SFranceschi](presentations/2017-07-21/general_track/foss4g-europe-2017-The_Geopaparazzi_Project_State_of_the_art-AAntonello-SFranceschi.pdf) |
| [ZOO-Project 1.7.0 : What's new on the open WPS platform - NBandara, GFenoy, NBozon, VRaghavan](presentations/2017-07-21/general_track/foss4g-europe-2017-ZOO-Project_1_7_0_Whats_new_on_the_open_WPS_platform-NBandara-GFenoy-NBozon-VRaghavan.pdf) |
| *Academic Track* |
| [A new strategy for DSM generation from satellite imagery with FOSS4G date results analysis on the ISPRS benchmark - MDiRita, ANascetti, MCrespi](presentations/2017-07-21/academic_track/foss4g-europe-2017-A_new_strategy_for_DSM_generation_from_satellite_imagery_with_FOSS4G_date_results_analysis_on_the_ISPRS_benchmark-MDiRita-ANascetti-MCrespi.pdf) |
| [Application of a pattern recognition algorithm for single tree detection from lidar data - AAntonello, SFranceshi, VFloreancig, FComiti, GTonon](presentations/2017-07-21/academic_track/foss4g-europe-2017-Application_of_a_pattern_recognition_algorithm_for_signle_tree_detection_from_lidar_data-AAntonello-SFranceshi-VFloreancig-FComiti-GTonon.pdf) |
| [Comparative analysis of 3D point clouds generated from a freeware and terrestrial laser scanner - KRDayal, SRaghavendra, HPande, PSTiwari, IChauhan](presentations/2017-07-21/academic_track/foss4g-europe-2017-Comparative_analysis_of_3D_ppoint_clouds_generated_from_a_freeware_and_terrestrial_laser_scanner-KRDayal-SRaghavendra-HPande-PSTiwari-IChauhan.pdf) |
| [Detecting stressed and pest-affected trees in aerial photos through machine learning : a proof of concept - MDiLeo, LMartinez, YChemin, P-JZarco-Tejada, BdeLaFutente, Martin-BPieterSA](presentations/2017-07-21/academic_track/foss4g-europe-2017-Detecting_stressed_and_pest-affected_trees_in_aerial_photos_through_machine_learning_a_proof_of_concept-MDiLeo-LMartinez-YChemin-P-JZarco-Tejada-BdeLaFutente-Martin-BPieterSA.pdf) |
| [Detecting stressed and pest-affected trees in aerial photos through machine learning a proof of concept - PWN_P T_outbreak_v2](presentations/2017-07-21/academic_track/foss4g-europe-2017-Detecting_stressed_and_pest-affected_trees_in_aerial_photos_through_machine_learning_a_proof_of_concept-PWN_PT_outbreak_v2.gif) |
| [Migrate a FOSS web mapping application for educating and raising awareness about migration flows in Europe - MABrovelli, MMinghini, CEKilsedar, MZurbaran, MAiello, MGianinetto](presentations/2017-07-21/academic_track/foss4g-europe-2017-Migrate_a_FOSS_web_mapping_application_for_educating_and_raising_awareness_about_migration_flows_in_Europe-MABrovelli-MMinghini-CEKilsedar-MZurbaran-MAiello-MGianinetto.pdf) |
| [Query support for GMZ - AKhandelwal, KSRajan](presentations/2017-07-21/academic_track/foss4g-europe-2017-Query_support_for_GMZ-AKhandelwal-KSRajan.pdf) |

## Topic Talks

| topic-talks |
|:-----|
| *Topic Talks* |
| [FOSS4GEurope welcomes new volunteers to OSGeo_community](presentations/topic-talks/foss4g-europe-2017-TopicTalk-FOSS4GEurope_welcomes_new_volunteers_to_OSGeo_community.pdf) |
| [Incubation Orientation](presentations/topic-talks/foss4g-europe-2017-TopicTalk-IncubationOrientation.pdf) |
| [OSGeo-LiveforINSPIRE - Plan](presentations/topic-talks/foss4g-europe-2017-TopicTalk-OSGeo-LiveforINSPIRE-Plan.pdf) |
| [OSGeo-LiveforINSPIRE - Introduction](presentations/topic-talks/foss4g-europe-2017-TopicTalk-OSGeo-LiveforINSPIRE-Introduction.pdf) |
| [OsGeo-LiveforINSPIRE - Status](presentations/topic-talks/foss4g-europe-2017-TopicTalk-OsGeo-LiveforINSPIRE-Status.pdf) |
| [OSGeo-LiveforINSPIRE - Data Provider Wish List - BRGM](presentations/topic-talks/foss4g-europe-2017-TopicTalk-OSGeo-LiveforINSPIRE-DataProviderWishList.pdf) |
| [OSGeo-LiveforINSPIRE - Discovery Services - Geonetwork](presentations/topic-talks/foss4g-europe-2017-TopicTalk-OSGeo-LiveforINSPIRE-DiscoveryServicesGeonetwork.pdf) |
| [OSGeo-LiveforINSPIRE - eReporting - 52North](presentations/topic-talks/foss4g-europe-2017-TopicTalk-OSGeo-LiveforINSPIRE-eReporting-52North.pdf) |
| [OsGeo-LiveforINSPIRE - geOrchestra](presentations/topic-talks/foss4g-europe-2017-TopicTalk-OsGeo-LiveforINSPIRE-geOrchestra.pdf) |

## Contributing

If you want to contribute to add a missing presentation, fixing a typo; please
clone the repository and send your changes with a pull request.

## License

All presentations are licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

![Creative Commons BY-SA](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

See the [LICENSE.md](LICENSE.md) file for details.

